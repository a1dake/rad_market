import sys
import requests
import cfscrape
import urllib3
from urllib3 import exceptions
from lxml import html
import sqlite3
import os
from datetime import datetime, timedelta
sys.path.append("/home/manage_report")
currentdir = os.path.dirname(os.path.realpath(__file__))
db_path = os.path.join(currentdir, "rad.db")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def create_database(self):
        try:
            conn = sqlite3.connect(db_path)
            sql = conn.cursor()
            sql.execute("""CREATE TABLE orders (url text);""")
            conn.close()
        except:
            pass

    def insert_data(self, value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("INSERT INTO orders(url) VALUES (?)", [value])
        conn.commit()
        conn.close()

    def search_data(self, search_value):
        conn = sqlite3.connect(db_path)
        c = conn.cursor()
        c.execute("SELECT * FROM orders WHERE url = ?", (search_value,))
        rows = c.fetchall()
        if rows:
            conn.close()
            return True
        else:
            conn.close()
            return False

    def get_data(self):
        self.create_database()

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://market.lot-online.ru/searchServlet?query=%7B%22types%22%3A%5B%22SMALL_PURCHASE%22%2C%22ELECTRONIC_STORE%22%5D%7D&filter=%7B%22state%22%3A%5B%22GD%22%2C%22CURRENT%22%5D%7D&sort=%7B%22placementDate%22%3Afalse%7D&limit=%7B%22min%22%3A0%2C+%22max%22%3A20%2C+%22updateTotalCount%22%3Atrue%7D'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://gz.lot-online.ru/etp_front/', verify=False)

        json_data = 'query=%7B%22types%22%3A%5B%22SMALL_PURCHASE%22%2C%22ELECTRONIC_STORE%22%5D%7D&filter=%7B%22state%22%3A%5B%22GD%22%2C%22CURRENT%22%5D%7D&sort=%7B%22placementDate%22%3Afalse%7D&limit=%7B%22min%22%3A0%2C+%22max%22%3A20%2C+%22updateTotalCount%22%3Atrue%7D'
        response = session.get(url, json=json_data, verify=False)
        data = response.json()
        contents = self.get_content(data)
        return contents

    def get_content(self, data):
        orders = []
        try:
            for item in data.get("list"):
                item_data = {
                    'type': 2,
                    'title': '',
                    'purchaseNumber': '',
                    'fz': 'ЗМО',
                    'purchaseType': '',
                    'url': '',
                    'lots': [],
                    'attachments': [],
                    'procedureInfo': {
                        'endDate': ''
                    },
                    'customer': {
                        'fullName': '',
                        'factAddress': '',
                        'inn': 0
                    }}
                item_data['url'] = link = str(item.get("offerLink"))
                response = self.session.get(link, timeout=30)
                tree = html.document_fromstring(response.content)

                item_data['title'] = str(self.get_title(item))
                item_data['purchaseNumber'] = str(self.get_purchase_number(item))
                item_data['purchaseType'] = str(self.get_purchase_type(item))

                item_data['customer']['fullName'] = str(self.get_full_name(item))
                item_data['customer']['inn'] = int(self.get_inn(item))

                item_data['procedureInfo']['endDate'] = self.get_end_date(item)

                item_data['lots'] = self.get_lots(tree, item)

                item_data['attachments'] = self.get_files(tree)

                db_search = self.search_data(link)
                if db_search:
                    continue
                else:
                    orders.append(item_data)
                    self.insert_data(link)
        except Exception as e:
            print(f'{e} - Ошибка в получении данных')
        return orders

    def get_title(self, data):
        try:
            title = ' '.join(data.get("title").split())
        except:
            title = ''
        return title

    def get_purchase_type(self, data):
        try:
            purchase_type = data.get("placementType")
        except:
            purchase_type = ''
        return purchase_type

    def get_purchase_number(self, data):
        try:
            number = data.get("filingNumber")
        except:
            number = ''
        return number

    def get_end_date(self, data):
        try:
            date = data.get("gdEndDate")[0:16]
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date

    def get_full_name(self, data):
        try:
            organizer_data = data.get("organizer")
            name = organizer_data.get("title")
        except:
            name = ''
        return name

    def get_inn(self, data):
        try:
            organizer_data = data.get("organizer")
            inn = organizer_data.get("inn")
            if not inn or inn is None:
                inn = 0
        except:
            inn = 0
        return inn

    def get_lots(self, tree, data):
        try:
            lots_data = []
            lots = {
                'address': '',
                'price': '',
                'lotItems': []
            }

            lots['address'] = self.get_lot_adress(tree)
            lots['price'] = self.get_lot_price(tree)
            title = self.get_lot_title(data)

            names = {
                'code': '1',
                'name': title}

            lots['lotItems'].append(names)
            lots_data.append(lots)
        except:
            return []
        return lots_data

    def get_lot_adress(self, tree):
        try:
            adress = ''.join(tree.xpath('//*[contains(text(),"Регион и адрес поставки")]/parent::*/text()[2]')).strip()
        except:
            adress = ''
        return adress

    def get_lot_price(self, tree):
        try:
            price = ''.join(''.join(tree.xpath('//*[contains(text(),"Стоимость мин. партии (без НДС и с НДС)")]/parent::*/parent::*/./*[6]/./*[2]/text()')).split("\xa0"))
        except:
            price = ''
        return price

    def get_lot_title(self, data):
        try:
            title = ' '.join(data.get("title").split())
        except:
            title = ''
        return title

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y %H:%M')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date

    def get_files(self, tree):
        data = []
        try:
            files = tree.xpath('//div[@id="Any_5"]/div/table/tbody/tr/td/a')
            for file in files:
                name = ' '.join(''.join(file.xpath('text()')).split())
                url = 'https://market.lot-online.ru' + ''.join(file.xpath('@href'))
                doc = {'docDescription': '',
                       'url': ''}
                doc['docDescription'] = name
                doc['url'] = url
                data.append(doc)
        except:
            return []
        return data
